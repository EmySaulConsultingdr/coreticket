﻿using AutoMapper;
using CoreTicket.Dtos.Product;
using Database.Interfaces;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreTicket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : TBaseController<Product, ProductDto, ProductDto, ProductDto>
    {
        public ProductController(IMapper mapper, IRepositoryBase<Product> repositoryBase) : base(mapper, repositoryBase)
        {
        }
    }
}
