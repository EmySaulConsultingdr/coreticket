﻿using AutoMapper;
using CoreTicket.Dtos.Table;
using Database.Interfaces;
using Database.Models;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CoreTicket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TableController : TBaseController<Table, TableDto, TableDto, TableDto>
    {
        IMapper mapper;
        public TableController(IMapper mapper, IRepositoryBase<Table> repositoryBase) : base(mapper, repositoryBase)
        {
            this.mapper = mapper;
        }

        public override long Create([FromBody] TableDto currentTicket)
        {
            currentTicket.CreationTime = DateTime.Now;
            currentTicket.Status = TableStatus.Available;
            var table = mapper.Map<Table>(currentTicket);

            return _repositoryBase.Create(table).Id;
        }
    }
}
