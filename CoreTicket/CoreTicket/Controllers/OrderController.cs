﻿using AutoMapper;
using CoreTicket.Dtos.Orders;
using Database.Interfaces;
using Database.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreTicket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : TBaseController<Order, GetOrderDto, CreateOrderDto, UpdateOrderDto>
    {
        readonly IMapper _mapper;
        readonly IRepositoryBase<OrderDetail> repositoryDetail;
        readonly IRepositoryBase<Table> repositoryTable;
        readonly IRepositoryBase<Product> repositoryProduct;

        public OrderController(IMapper mapper, IRepositoryBase<Order> repositoryBase,
            IRepositoryBase<Table> repositoryTable, IRepositoryBase<OrderDetail> repositoryDetail,
            IRepositoryBase<Product> repositoryProduct) 
            : base(mapper, repositoryBase)
        {
            _mapper = mapper;
            this.repositoryDetail = repositoryDetail;
            this.repositoryTable = repositoryTable;
            this.repositoryProduct = repositoryProduct;
        }
        
        [HttpGet]
        [Route("splitPayment/{id}")]
        public ActionResult SplitPayment(int id) 
        {
            var order = _repositoryBase.FindAll("OrderDetails")
                .FirstOrDefault(x => x.Id == id);

            order.Status = Status.Cancelado;
            _repositoryBase.Update(order);
            FreeTable(order);

            if(order != null)
            {

                var listNames = new List<string>();
                foreach (var orderDetail in order.OrderDetails)
                {
                    if (!listNames.Contains(orderDetail.CustomerName))
                        listNames.Add(orderDetail.CustomerName);
                }


                var ordersArr = new Order[listNames.Count];

                for (int i = 0; i < ordersArr.Length; i++)
                {
                    var newOrder = new Order();
                    
                    newOrder.Status = Status.PendientePago;

                    

                    newOrder.OrderDetails = new List<OrderDetail>();

                    foreach (var orderDetail in order.OrderDetails)
                    {
                        if(orderDetail.CustomerName == listNames[i])
                        {
                            newOrder.OrderDetails.Add(orderDetail);
                        }
                    }

                    newOrder = GetTotalPayment(newOrder);
                    _repositoryBase.Create(newOrder);

                }
                return Ok();
            }
            return NotFound();
        }

        [HttpGet]
        [Route("onePayment/{id}")]
        public ActionResult OnePayment(int id)
        {
            var order = _repositoryBase.FindAll("OrderDetails")
                .FirstOrDefault(x => x.Id == id);

            FreeTable(order);

            if (order != null)
            {
                order.Status = Status.PendientePago;
                order = GetTotalPayment(order);
                _repositoryBase.Update(order);

                return Ok();
            }

            return NotFound();
        }

        private Order GetTotalPayment(Order order)
        {
            int sum = 0;

            foreach (var orderDetail in order.OrderDetails)
            {
                var product = repositoryProduct.FindAll()
                    .First(x => x.Id == orderDetail.ProductId);
                sum += (product.Price * orderDetail.Quantity);
            }

            order.SubTotal = sum;
            order.Itbis = sum * 0.28;
            order.Total = order.Itbis + order.SubTotal;

            return order;
        }

        private void FreeTable(Order order)
        {
            var details = order.OrderDetails.ToArray();
            int tableId = details[0].TableId;
            var table = repositoryTable.FindAll()
                    .First(x => x.Id == tableId);

            table.Status = TableStatus.Available;

            repositoryTable.Update(table);
        }
        public override long Create([FromBody] CreateOrderDto currentTicket)
        {
            var order = new Order();
            order.Status = Status.Abierta;
            order.CreationTime = DateTime.Now;
            var orderId = _repositoryBase.Create(order).Id;

            var table = repositoryTable.FindAll()
                    .First(x => x.Id == currentTicket.TableId);

            table.Status = TableStatus.Occupied;

            repositoryTable.Update(table);

            foreach (var orderDetailDto in currentTicket.OrderDetails)
            {
                var orderDetail = _mapper.Map<OrderDetail>(orderDetailDto);
                orderDetail.OrderId = orderId;
                orderDetail.TableId = currentTicket.TableId;

                

                repositoryDetail.Create(orderDetail);
            }

            return orderId;
        }

        [HttpGet]
        [Route("table/{id}")]
        public ActionResult<GetOrderDto> GetOrderByMesaId(int id)
        {
            var ordersListOpen = _repositoryBase.FindAll(x => x.OrderDetails)
                .Where(x => x.Status == Status.Abierta);
            
            Order order = null;

            foreach (var orderOpen in ordersListOpen)
            {
                var details = orderOpen.OrderDetails.ToArray();

                if (details.Length > 0)
                {
                    if(details[0].TableId == id)
                    {
                        order = orderOpen;
                        break;
                    }
                }   
            }

            if (order != null)
            {
                var orderDto = _mapper.Map<GetOrderDto>(order);
                return Ok(orderDto);
            }
                

            return NotFound();
        }

        [HttpGet]
        [Route("pending")]
        public ActionResult<List<PendingOrderDto>> GetPendingOrders()
        {
            var ordersList = _repositoryBase.FindAll(x => x.OrderDetails)
                .Where(x => x.Status == Status.PendientePago).ToList();
            
            
            var ordersListDto = new List<PendingOrderDto>();

            if(ordersList.Count > 0)
            {
                foreach (var order in ordersList)
                {
                    var detail = repositoryDetail.FindAll()
                        .First(x => x.OrderId == order.Id);

                    var orderDto = _mapper.Map<PendingOrderDto>(order);
                    orderDto.TableId = detail.TableId;
                    orderDto.CustomerName = detail.CustomerName;

                    ordersListDto.Add(orderDto);
                }
            }
            return ordersListDto;
        }

        [HttpPut]
        [Route("pay/{id}")]
        public ActionResult GetPendingOrders(int id)
        {
            var order = _repositoryBase.FindAll()
                .FirstOrDefault(x => x.Id == id);

            if(order != null)
            {
                order.Status = Status.Pago;
                _repositoryBase.Update(order);
            }

            return Ok();
        }


    }
}
