﻿using AutoMapper;
using CoreTicket.Dtos.OrderDetail;
using Database.Interfaces;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreTicket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderDetailsController : TBaseController<OrderDetail, OrderDetailDto, OrderDetailDto, OrderDetailDto>
    {
        public OrderDetailsController(IMapper mapper, IRepositoryBase<OrderDetail> repositoryBase) : base(mapper, repositoryBase)
        {
        }

        
    }
}
