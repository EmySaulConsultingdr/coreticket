﻿using AutoMapper;
using CoreTicket.Dtos.OrderDetail;
using CoreTicket.Dtos.Orders;
using CoreTicket.Dtos.Product;
using CoreTicket.Dtos.Table;
using CoreTicket.Dtos.Users;
using Database.Models;

namespace CoreTicket
{
    internal class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
            CreateMap<User, UserCreateDto>();
            CreateMap<UserCreateDto, User>();


            CreateMap<Order, GetOrderDto>();
            CreateMap<UpdateOrderDto, Order>();
            CreateMap<CreateOrderDto, Order>();
            CreateMap<Order, PendingOrderDto>();

            CreateMap<OrderDetail, OrderDetailDto>();
            CreateMap<OrderDetailDto, OrderDetail>();
            CreateMap<CreateOrderDetailDto, OrderDetail>();


            CreateMap<Table, TableDto>();
            CreateMap<TableDto, Table>();

            CreateMap<Product, ProductDto>();
            CreateMap<ProductDto, Product>();
        }
    }
}