﻿
using Database.Models;

namespace CoreTicket.Dtos.Table
{
    public class TableDto: TEntityDto
    {
        public TableStatus Status { get; set; }
        public int Chairs { get; set; }
    }
}
