﻿

namespace CoreTicket.Dtos.Orders
{
    public class PendingOrderDto: TEntityDto
    {
        public int TableId { get; set; }
        public string CustomerName { get; set; }
        public int SubTotal { get; set; }
        public decimal? Itbis { get; set; }
        public decimal? Total { get; set; }
    }
}
