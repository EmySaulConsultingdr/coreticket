﻿

namespace CoreTicket.Dtos.Orders
{
    public class UpdateOrderDto: TEntityDto
    {
        public int SubTotal { get; set; }
        public decimal? Itbis { get; set; }
        public decimal? Total { get; set; }
        public string Status { get; set; }
    }
}
