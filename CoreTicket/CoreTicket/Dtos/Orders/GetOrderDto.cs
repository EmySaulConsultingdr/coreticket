﻿using CoreTicket.Dtos.OrderDetail;
using System.Collections.Generic;

namespace CoreTicket.Dtos.Orders
{
    public class GetOrderDto : TEntityDto
    {
        public int SubTotal { get; set; }
        public decimal? Itbis { get; set; }
        public decimal? Total { get; set; }
        public string Status { get; set; }
        public List<OrderDetailDto> OrderDetails { get; set; }
    }
}
