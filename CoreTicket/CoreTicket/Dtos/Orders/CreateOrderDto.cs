﻿
using CoreTicket.Dtos.OrderDetail;
using System.Collections.Generic;

namespace CoreTicket.Dtos.Orders
{
    public class CreateOrderDto: TEntityDto
    {
        public CreateOrderDto()
        {
            OrderDetails = new List<CreateOrderDetailDto>();
        }

        public int TableId { get; set; }

        public List<CreateOrderDetailDto> OrderDetails { get; set; }
    }
}
