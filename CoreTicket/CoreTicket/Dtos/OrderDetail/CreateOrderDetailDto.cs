﻿
namespace CoreTicket.Dtos.OrderDetail
{
    public class CreateOrderDetailDto : TEntityDto
    {
        public int ProductId { get; set; }

        public int Quantity { get; set; }
        public string CustomerName { get; set; }
    }
}
