﻿
namespace CoreTicket.Dtos.OrderDetail
{
    public class OrderDetailDto : TEntityDto
    {
        public int OrderId { get; set; }

        public int ProductId { get; set; }

        public int TableId { get; set; }

        public int Quantity { get; set; }
        public string CustomerName { get; set; }
    }
}
