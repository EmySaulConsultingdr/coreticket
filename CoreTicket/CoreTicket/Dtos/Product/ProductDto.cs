﻿

namespace CoreTicket.Dtos.Product
{
    public class ProductDto : TEntityDto
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string Photo { get; set; }
    }
}
