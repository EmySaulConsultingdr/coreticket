﻿namespace CoreTicket.Dtos.Users
{
    public class UserCreateDto : TEntityDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }

    }
}
