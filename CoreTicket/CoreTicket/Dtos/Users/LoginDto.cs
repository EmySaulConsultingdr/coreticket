﻿using Database.Models;

namespace CoreTicket.Dtos.Users
{
    public class LoginDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public UserType Type { get; set; }
    }
}