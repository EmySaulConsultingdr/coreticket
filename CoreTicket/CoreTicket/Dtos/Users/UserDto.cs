﻿
using Database.Models;
using System.Collections.Generic;

namespace CoreTicket.Dtos.Users
{
    public class UserDto : TEntityDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public UserType Type { get; set; }
    }
}
