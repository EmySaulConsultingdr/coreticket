﻿using System;

namespace CoreTicket.Dtos
{
    public class TEntityDto
    {
        public int? Id { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

    }
}
