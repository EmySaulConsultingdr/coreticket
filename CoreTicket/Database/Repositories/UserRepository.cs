﻿using Database.Interfaces;
using Database.Models;

namespace Database.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(ContactsContext ticketContext) : base(ticketContext)
        {
        }
    }
}
