﻿
namespace Database.Models
{
    public enum UserType
    {
        Admin = 1,
        Chef = 2,
        Waiter = 3,
        Cashier = 4
    }
}
