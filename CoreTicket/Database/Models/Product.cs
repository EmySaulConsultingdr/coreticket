﻿using System.Collections.Generic;

namespace Database.Models
{
    public class Product : Entity
    {
        public Product()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public string Name { get; set; }
        public int Price { get; set; }
        public string Photo { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
