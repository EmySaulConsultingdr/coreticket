﻿using System.Collections.Generic;

namespace Database.Models
{
    public class Table : Entity
    {
        public Table()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public TableStatus Status { get; set; }
        public int Chairs { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
    public enum TableStatus
    {
        Available = 1,
        Occupied = 2,
        Reserved = 3
    }
}
