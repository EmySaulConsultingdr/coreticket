﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Models
{
    public class OrderDetail : Entity
    {
        public int OrderId { get; set; }
        [ForeignKey(nameof(OrderId))]
        public virtual Order Order { get; set; }

        public int ProductId { get; set; }
        [ForeignKey(nameof(ProductId))]
        public virtual Product Product { get; set; }

        public int TableId { get; set; }
        [ForeignKey(nameof(TableId))]
        public virtual Table Table { get; set; }

        public int Quantity { get; set; }
        public string CustomerName { get; set; }
    }
}
