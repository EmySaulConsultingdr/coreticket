﻿
namespace Database.Models
{
    public enum Status
    {
        Abierta = 1,
        Cancelado = 2,
        Pago = 3,
        PendientePago = 4
    }
}
