﻿using System;
using System.Collections.Generic;

namespace Database.Models
{
    public class Order : Entity
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }
        public int SubTotal { get; set; }
        public double Itbis { get; set; }
        public double Total { get; set; }
        public Status Status { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

    }
}
