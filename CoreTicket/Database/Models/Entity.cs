﻿using System;

namespace Database.Models
{
    public class Entity
    {
        public int Id { get; set; }

        public DateTime CreationTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
    }
}
